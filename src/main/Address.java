package main;

import java.io.Serializable;

/**
 * This class contains all data pertaining to an address, such as used by the Contact class.
 */
public class Address implements Serializable{
	private String street;
	private String postcode;
	private String city;
	private String country;

	public Address(String street, String postcode, String city, String country) {
		this.street = street;
		this.postcode = postcode;
		this.city = city;
		this.country = country;
	}

	@Override
	public String toString() {
		return street + ", " + postcode + ", " + city + ", " + country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
