package main;

public class ArrayException extends Exception {

    String exceptionMessage;
    
    public ArrayException(String msg) {
        super(msg);
        this.exceptionMessage = msg;
    }

    public void setMessage(String msg) {
        exceptionMessage = msg;
    }

    @Override
    public String toString() {
        return exceptionMessage;
    }

}
