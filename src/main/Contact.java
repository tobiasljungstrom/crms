package main;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * This class contains all information pertaining to a listable contact.
 */
public class Contact implements Listable, Serializable {
	private String name;
	private Address address;
	private String email;
	private String company;
	private Calendar birthdate;
	private ArrayList<Relation> relations;

	public Contact(String name, Address address, String email, String company, Calendar birthdate) {
		this.name = name;
		this.address = address;
		this.email = email;
		this.company = company;
		this.birthdate = birthdate;

		this.relations = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Calendar getDate() {
		return birthdate;
	}

	public void setDate(Calendar birthdate) {
		this.birthdate = birthdate;
	}

	public ArrayList<Relation> getRelations() {
		return relations;
	}

	public void addRelation(Relation relation) {
		relations.add(relation);
	}

	public void removeRelation(int index) {
		relations.remove(index);
	}

	@Override
	public String toString() {
		return name + ", " + company;
	}

	@Override
	/**
	 * Prints all the information pertaining to the class, over several rows.
	 */
	public void view() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd");
		System.out.println("Name: " + name + "\n" +
				"Address: " + address + "\n" +
				"E-mail: " + email + "\n" +
				"Company: " + company + "\n" +
				"Birth date: " + dateFormat.format(birthdate.getTime()));
	}
}
