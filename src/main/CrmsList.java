package main;

import java.io.Serializable;
import java.util.Iterator;
import java.util.RandomAccess;

/**
 *
 * @param <T> generic array class to handle Listable T items
 */
public class CrmsList<T extends Listable> implements Iterable<Listable>, RandomAccess, Serializable {

    // array capacity, the real size of this array
    // hidden to the user to improve performance
    private int capacity;

    // logical size, presented to the user
    private int logicalSize;

    // the list to work with
    private Listable[] list;

    // an exception to handle ArrayExceptions
    private transient ArrayException arrayException;

    // exception message constant
    private transient final String exceptionMessage = "ArrayException. Trying to access item with index: ";

    /**
     *
     * @param capacity setting initial capacity
     */
    public CrmsList(int capacity) {
        this.capacity = capacity;
        logicalSize = 0;
        list = new Listable[capacity];
        arrayException = new ArrayException("");

        // an iterator for this class
        // end of the iterator object
    }

    /**
     * setting initial capacity to 50
     */
    public CrmsList() {
        this(50);
    }

    /**
     * increase size if the capacity is reached
     *
     * @param item
     */
    public void add(T item) {

        // double the list size if the list items adds up to the capacity
        if (logicalSize == capacity) {

            // create a new array with the capacity * 2
            Listable[] tempList = new Listable[capacity * 2];
            // copy items from original
            System.arraycopy(list, 0, tempList, 0, capacity);

            // point the original back to the copy
            list = tempList;

            // double the capacity
            capacity *= 2;
        }

        // set last presentable(logical) index to the value of T item
        list[logicalSize] = item;

        // increase logical size
        logicalSize++;
    }

    /*
     * only for testing
     */
//    public int TotalSize() {
//        return capacity;
//    }
    /**
     *
     * @param index, remove T item at index
     */
    public void remove(int index) {
        // throw an exception if the index is out of logical bounds
        indexHandler(index);

        // shift every index to the left one step
        // to remove the given index
        for (int i = index; i < logicalSize - 2; i++) {
            list[i] = list[i + 1];
        }

        // decrease the logical size
        logicalSize--;
    }

    /**
     *
     * @param index chosen index
     * @param item item to replace at the given index
     */
    public void set(int index, T item) {
        // throw an exception if the index is out of logical bounds
        indexHandler(index);

        // replace value at index with the T item
        list[index] = item;
    }

    /**
     *
     * @return the logical size presented to the user
     */
    public int size() {
        return logicalSize;
    }

    /**
     *
     * @param index index if this list
     * @return T item at index
     */
    public Listable get(int index) {
        // throw an exception if the index is out of logical bounds
        indexHandler(index);
        return list[index];
    }

    /**
     *
     * @param index view T item at index
     */
    public void viewItem(int index) {
        // throw an exception if the index is out of logical bounds
        indexHandler(index);
        // invoke the T item at index's version of view()
        list[index].view();
    }

    /**
     * clear this array
     */
    public void clear() {
        logicalSize = 0;
    }

    /**
     *
     * @return return whether this array is empty or not
     */
    public boolean isEmpty() {
        return logicalSize == 0;
    }

    /**
     * List all items in this collection
     */
    public void listAllItems() {
        for (int i = 0; i < logicalSize; i++) {
            System.out.println(list[i]);
        }
    }

    /**
     *
     * @return return a String array of this array
     */
    public String[] toStringArray() {

        String[] newList = new String[logicalSize];

        // copy every logical element from list
        // to the new list
        for (int i = 0; i < logicalSize; i++) {
            newList[i] = list[i].toString();
        }
        // return the new list
        return newList;

    }

    /**
     *
     * @return return this array to normal [] array
     */
    public Listable[] toArray() {

        Listable[] newList = new Listable[logicalSize];

        // copy every logical element from list
        // to the new list
        System.arraycopy(list, 0, newList, 0, logicalSize);

//        // this is practically the same as the system copy above
//        for (int i = 0; i < logicalSize; i++) {
//
//            newList[i] = list[i];
//        }
        // return the new list
        return newList;
    }

    /**
     *
     * @return return this class's iterator
     */
    @Override
    public Iterator<Listable> iterator() {
        Iterator iterator = new Iterator() {
            private int it = 0;

            /*
             * resets this iterator to be able to
             * re-iterate this collection
             */
            @Override
            public boolean hasNext() {
                boolean ret = it < logicalSize;
                // reset this iterator if it has come to the end
                if (!ret) {
                    reset();
                }
                return ret;
            }

            @Override
            public Object next() {
                // return list[it] and increase it by 1
                return list[it++];
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            // reset this iterator
            public void reset() {
                it = 0;
            }
        };
        return iterator;
    }

    /*
     * handle index exceptions for this class
     */
    private void indexHandler(int index) {
        // throw an exception if the index is out of logical bounds
        if (index >= this.logicalSize) {
            // print the generated exception message.
            // using System.err intead of System.out
            System.err.println(exceptionMessage + index);
            // set exception message
            arrayException.setMessage(exceptionMessage + index);

            try {
                // throw the exception
                throw arrayException;
            } catch (ArrayException ex) {
                ex.printStackTrace();
                System.exit(0);
            }
        }
    }
}
