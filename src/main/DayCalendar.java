package main;

import java.io.Serializable;
import java.util.Calendar;

public class DayCalendar implements Serializable{

    protected boolean isToday(Calendar calendar) {

        Calendar c = backToMidnight(calendar);
        Calendar today = backToMidnight(Calendar.getInstance());

        return c.equals(today);

    }

    protected boolean beforeToday(Calendar calendar) {

        Calendar c = backToMidnight(calendar);
        Calendar today = backToMidnight(Calendar.getInstance());

        return c.before(today);
    }

    // defined in terms of evaluation of ((NOT beforeToday) AND (NOT isToday))
    protected boolean afterToday(Calendar calendar) {
        return !isToday(calendar) && !beforeToday(calendar);
    }

    // returns a Calendar based in the parameter
    // which has hour, minute, second, and millisend set to 0
    public Calendar backToMidnight(Calendar calendar) {

        // clone "calendar" to prevent pass-by-value-of-reference
        // so we only handle a copy of the parameter
        Calendar c = (Calendar) calendar.clone();

        // reset all necessary values, to only check
        // the day
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c;
    }
    
}
