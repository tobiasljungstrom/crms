package main;

import java.util.Calendar;

public interface Listable {

    void view();
    Calendar getDate();

}
