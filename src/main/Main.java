package main;

import tools.ReadWrite;

/**
 *The entry point for the program.
 */
public class Main {

	public static void main(String[] args) {
		ReadWrite readerWriter = new ReadWrite("database/data.file");

		Program program = readerWriter.read();
//		Program program = new Program();
		program.run();
		readerWriter.write(program);
	}
}
