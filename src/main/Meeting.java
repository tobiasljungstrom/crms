package main;

import menuHandling.Option;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * This class contains all information relevant to a meeting.
 */
public class Meeting implements Listable, Serializable {

    private String title;
    private Contact contact;
    private Calendar date;
    private String journal;
    private DayCalendar dayCalendar;

    public Meeting(String title, Contact contact, Calendar date, String journal) {
        this.title = title;
        this.contact = contact;
        this.date = date;
        this.journal = journal;

        dayCalendar = new DayCalendar();

//        // we dont want to make it able
//        // to create/or delete a meeting in the past...
//        if (dayCalendar.beforeToday(date)) {
//            try {
//                throw new Exception("Can't book a meeting with a date in the past");
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public boolean hasOccurred() {
        return dayCalendar.beforeToday(date);
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd");
        return "Meeting with: " + this.contact + " at: " + dateFormat.format(this.date.getTime());
    }

    @Override
    public void view() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd");
        System.out.println(dateFormat.format(this.date.getTime()) + ": " + this.title + "\n"
                + "Contact: " + this.contact + "\n"
                + "Notes: " + this.journal + "\n"
        );
    }
}
