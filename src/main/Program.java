package main;

import tools.ConsoleInput;
import tools.ConsoleMenu;
import tools.Menu;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * This is the main program class. It contains a menu for accessing the different parts of the program.
 */
public class Program implements Serializable {

	private CrmsList<Contact> contactList;
	private CrmsList<Meeting> meetingList;

	private transient ConsoleInput input;
	private transient SimpleDateFormat standardFormat;

	public Program() {
		contactList = new CrmsList<>();
		meetingList = new CrmsList<>();
	}

	public void run() {

		System.out.println("--- Program Start ---");

		checkBirthdays();

		input = new ConsoleInput();
		standardFormat = new SimpleDateFormat("yyyy-MM-dd");

		Menu mainMenu = new ConsoleMenu(
				"Main Menu",
				"Contacts",
				"Meetings"
		);

		boolean loop = true;

		while (loop) {
			mainMenu.displayMenu();

			switch (mainMenu.makeSelection()) {
				case 1:
					contacts();
					break;
				case 2:
					meetings();
					break;
				default:
					loop = false;
			}
		}

		System.out.println("\n--- Program End ---");
	}

	private void checkBirthdays() {

		Calendar birth = new GregorianCalendar();
		Contact contact;

		DayCalendar dc = new DayCalendar();
		Calendar max = dc.backToMidnight(Calendar.getInstance());
		max.add(Calendar.DAY_OF_YEAR, 7);

		Calendar min = dc.backToMidnight(Calendar.getInstance());
		min.add(Calendar.DAY_OF_YEAR, -7);

		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d");
		for (Listable l : contactList) {
			contact = (Contact) l;

			birth.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
			birth.set(Calendar.MONTH, contact.getDate().get(Calendar.MONTH));
			birth.set(Calendar.DAY_OF_MONTH, contact.getDate().get(Calendar.DAY_OF_MONTH));

			if (!birth.after(max) && !birth.before(min)) {
				System.out.println(contact.getName() + " has their birthday on " + dateFormat.format(birth.getTime()));
			}
		}

	}

	private void contacts() {
		Menu contactsMenu = new ConsoleMenu(
				"Contacts",
				"View contact",
				"New contact",
				"Edit contact",
				"Delete contact",
				"List all relations for a contact",
				"List all journals related to a contact");

		boolean loop = true;
		while (loop) {
			contactsMenu.displayMenu();

			switch (contactsMenu.makeSelection()) {
				case 1:
					viewContact();
					break;
				case 2:
					newContact();
					break;
				case 3:
					editContact();
					break;
				case 4:
					deleteContact();
					break;
				case 5:
					listRelations();
					break;
				case 6:
					listJournals();
					break;
				default:
					loop = false;
			}
		}
	}

	private void listJournals() {
		Contact currentContact = selectContact();

		for (int i = 0; i < meetingList.size(); i++) {
			Meeting tmpMeeting = (Meeting) meetingList.get(i);
			 if(tmpMeeting.getContact().equals(currentContact)) {
				 System.out.println(standardFormat.format(tmpMeeting.getDate().getTime()) + ": " + tmpMeeting.getJournal());
			 }

		}
	}

	private void listRelations() {

		Contact selectedContact = selectContact();
		for (int i = 0; i < selectedContact.getRelations().size(); i++) {
			System.out.println(selectedContact.getRelations().get(i));
		}
	}

	/**
	 * Creates a menu containing all contacts, then displays this menu, asks the user to choose a contact, and then deletes the chosen contact.
	 */
	private void deleteContact() {
		Menu allContactsMenu = new ConsoleMenu("Select contact to delete:", contactList.toStringArray());
		allContactsMenu.displayMenu();

		int selection = allContactsMenu.makeSelection();
		if (selection != 0) {
			contactList.remove(selection - 1);
		}
	}

	/**
	 * This class utilizes a temporary Contact object, whose fields can be altered, and which finally replaces the selected contact in the list.
	 */
	private void editContact() {
		Menu allContactsMenu = new ConsoleMenu("Select contact to edit:", contactList.toStringArray());
		allContactsMenu.displayMenu();

		int selection = allContactsMenu.makeSelection();
		Contact tmpContact = (Contact) contactList.get(selection - 1);

		if (selection != 0) {
			Menu editMenu = new ConsoleMenu("Select post to edit:", "Name", "Address", "Email", "Company", "Birth date", "Relations");

			boolean loop = true;

			while (loop) {
				editMenu.displayMenu();
				switch (editMenu.makeSelection()) {
					case 1:
						System.out.println("Current name: " + tmpContact.getName());
						tmpContact.setName(enterName());
						break;
					case 2:
						System.out.println("Current address: " + tmpContact.getAddress());
						tmpContact.setAddress(enterAddress());
						break;
					case 3:
						System.out.println("Current e-mail: " + tmpContact.getEmail());
						tmpContact.setEmail(enterEmail());
						break;
					case 4:
						System.out.println("Current company: " + tmpContact.getCompany());
						tmpContact.setCompany(enterCompany());
						break;
					case 5:
						System.out.println("Current birth date: " + standardFormat.format(tmpContact.getDate().getTime()));
						System.out.println("Please enter birth date (Year YYYY, Month MM, day DD):");
						tmpContact.setDate(enterDate());
						break;
					case 6:
						relations(tmpContact);
						break;
					default:
						loop = false;
				}
			}

			contactList.set(selection - 1, tmpContact);
		}
	}

	private void relations(Contact contact) {
		Menu relationsMenu = new ConsoleMenu("Select option", "Add relation", "Remove relation");

		boolean loop = true;

		while (loop) {
			relationsMenu.displayMenu();
			switch (relationsMenu.makeSelection()) {
				case 1:
					addRelation(contact);
					break;
				case 2:
					removeRelation(contact);
					break;
				default:
					loop = false;
			}
		}
	}

	private void removeRelation(Contact contact) {
		String[] relationList = new String[contact.getRelations().size()];
		for (int i = 0; i < contact.getRelations().size(); i++) {
			relationList[i] = contact.getRelations().get(i).toString();

		}
		Menu relationMenu = new ConsoleMenu("Select relation to delete", relationList);
		relationMenu.displayMenu();

		int selection = relationMenu.makeSelection();
		if (selection != 0) {
			contact.removeRelation(selection - 1);
		}
	}

	private void addRelation(Contact contact) {
		input = new ConsoleInput();

		String name;
		String relation;
		Calendar birthDate;

		System.out.println("Enter relation name:");
		name = input.getString();

		System.out.println("Enter type of relation (eg. husband):");
		relation = input.getString();

		System.out.println("Enter birth date (YYY, MM, DD");
		birthDate = enterDate();

		contact.addRelation(new Relation(name, relation, birthDate));

	}

	/**
	 * Asks the user for every single value that comprises a contact, then creates that contact and adds it to the contact list.
	 */
	private void newContact() {
		String name;
		Address address;
		String email;
		String company;
		Calendar birthdate;

		name = enterName();
		address = enterAddress();
		email = enterEmail();
		company = enterCompany();
		System.out.println("Enter birth date (YYYY, MM, DD): ");
		birthdate = enterDate();

		contactList.add(new Contact(name, address, email, company, birthdate));
	}

	private Calendar enterDate() {
		int year;
		int month;
		int day;


		year = input.getInt(1900, 9999);
		month = input.getInt(1, 12) - 1;
		day = input.getInt(1, 31);

		return new GregorianCalendar(year, month, day);
	}

	private String enterCompany() {
		System.out.println("Please enter company:");
		return input.getString();
	}

	private String enterEmail() {
		System.out.println("Please enter e-mail:");
		return input.getString();
	}

	private String enterName() {
		System.out.println("Please enter name:");
		return input.getString();
	}

	private Address enterAddress() {
		String street;
		String postcode;
		String city;
		String country;

		System.out.println("Please enter address (Street, Postcode, City, Country):");
		street = input.getString();
		postcode = input.getString();
		city = input.getString();
		country = input.getString();

		return new Address(street, postcode, city, country);
	}

	/**
	 * Creates a menu containing all contacts, then displays this menu, asks the user to choose a contact, and then displays the chosen contact.
	 */
	private void viewContact() {
		Menu allContactsMenu = new ConsoleMenu("Select contact to view:", contactList.toStringArray());
		allContactsMenu.displayMenu();

		int selection = allContactsMenu.makeSelection();
		if (selection != 0) {
			contactList.viewItem(selection - 1);
		}
	}

	private void meetings() {
		Menu meetingsMenu = new ConsoleMenu(
				"Meetings",
				"View meeting",
				"Schedule new meeting",
				"Edit existing meeting",
				"Delete meeting",
				"Check all meetings from a contact",
				"Get journal notes from a meeting");

		boolean loop = true;
		while (loop) {
			meetingsMenu.displayMenu();

			switch (meetingsMenu.makeSelection()) {
				case 1:
					viewMeeting();
					break;
				case 2:
					newMeeting();
					break;
				case 3:
					editMeeting();
					break;
				case 4:
					deleteMeeting();
					break;
				case 5:
					printAllMeetingsFromContact();
					break;
				case 6:
					getJournal();
					break;
				default:
					loop = false;
			}
		}
	}

	private void printAllMeetingsFromContact() {
		Contact contact = selectContact();
		Meeting m;
		for (Listable listable : meetingList) {
			m = (Meeting) listable;
			if (m.getContact().equals(contact)) {
				System.out.println(m);
			}
		}
	}

	private void getJournal() {
		Meeting meeting = selectMeeting();
		System.out.println(meeting.getJournal());
	}

	private void viewMeeting() {
		Menu allMeetingsMenu = new ConsoleMenu("Select meeting to view:", meetingList.toStringArray());
		allMeetingsMenu.displayMenu();

		int selection = allMeetingsMenu.makeSelection();
		if (selection != 0) {
			meetingList.viewItem(selection - 1);
		}
	}

	private Meeting selectMeeting() {
		Menu allContactsMenu = new ConsoleMenu("Select contact:", meetingList.toStringArray());

		allContactsMenu.displayMenu();
		int selection = allContactsMenu.makeSelection();

		return (Meeting) meetingList.get(selection - 1);
	}

	private void newMeeting() {
		String title;
		Contact contact;
		Calendar date;
		String journal;

		title = enterTitle();
		contact = selectContact();
		System.out.println("Enter date for meeting (Year YYYY, Month MM, day DD):");
		date = enterDate();
		journal = enterJournal();

		meetingList.add(new Meeting(title, contact, date, journal));
	}

	private String enterTitle() {
		System.out.println("Enter title: ");
		return input.getString();
	}

	private Contact selectContact() {
		Menu allContactsMenu = new ConsoleMenu("Select contact:", contactList.toStringArray());

		allContactsMenu.displayMenu();
		int selection = allContactsMenu.makeSelection();

		return (Contact) contactList.get(selection - 1);
	}

	private String enterJournal() {
		System.out.println("Enter journal notes for the meeting:");
		return input.getString();
	}

	private void editMeeting() {
		Menu allMeetingsMenu = new ConsoleMenu("Select meeting to edit:", meetingList.toStringArray());
		allMeetingsMenu.displayMenu();

		int selection = allMeetingsMenu.makeSelection();
		Meeting tmpMeeting = (Meeting) meetingList.get(selection - 1);

		if (selection != 0) {
			Menu editMenu = new ConsoleMenu("Select post to edit:", "Title", "Contact", "Date", "Journal notes");

			boolean loop = true;

			while (loop) {
				editMenu.displayMenu();
				switch (editMenu.makeSelection()) {
					case 1:
						System.out.println("Current title: " + tmpMeeting.getTitle());
						tmpMeeting.setTitle(enterTitle());
						break;
					case 2:
						System.out.println("Current contact: " + tmpMeeting.getContact());
						tmpMeeting.setContact(selectContact());
						break;
					case 3:
						System.out.println("Current date: " + tmpMeeting.getDate());
						System.out.println("Enter new date: ");
						tmpMeeting.setDate(enterDate());
						break;
					case 4:
						System.out.println("Current notes: " + tmpMeeting.getJournal());
						tmpMeeting.setJournal(enterJournal());
						break;
					default:
						loop = false;
				}
			}

			meetingList.set(selection - 1, tmpMeeting);
		}
	}

	private void deleteMeeting() {
		Menu allMeetingsMenu = new ConsoleMenu("Select meeting to delete:", meetingList.toStringArray());
		allMeetingsMenu.displayMenu();

		int selection = allMeetingsMenu.makeSelection();
		if (selection != 0) {
			meetingList.remove(-1);
		}
	}
}
