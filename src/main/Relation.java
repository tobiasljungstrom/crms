package main;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Tobias on 2015-27-10.
 */
public class Relation implements Listable, Serializable{

	private String name;
	private String relation;
	private Calendar birthDate;

	public Relation(String name, String relation, Calendar birthDate) {
		this.name = name;
		this.relation = relation;
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return name + ", " + relation + ", born " + dateFormat.format(birthDate.getTime());
	}

	@Override
	public void view() {
		System.out.println(toString());
	}

	@Override
	public Calendar getDate() {
		return birthDate;
	}
}
