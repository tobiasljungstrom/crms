package tools;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * The <code>UserInput</code> class uses a <code>Scanner</code> object to
 * receive input via the console. In addition to receiving multiple different
 * data types it also handles exceptions such as the user giving a different
 * data type than what was requested.
 *
 * @author Tobias Ljungstr&ouml;m
 */
public class ConsoleInput {

	/**
	 * Asks the user for an integer value and then returns it. If a different
	 * data type is received, the method will keep asking until an integer is
	 * entered.
	 *
	 * @return The value of the integer entered by the user
	 */
	public int getInt() {
		return getInt(-2147483648, 2147483647); //The low and high limit of the int data type.
	}

	/**
	 * Asks the user for an integer value and returns it if it is within the
	 * specified parameters. If a different data type is received, the method
	 * will keep asking until an integer is entered.
	 *
	 * @param lowLimit  The lowest number to be accepted
	 * @param highLimit The highest number to be accepted
	 * @return The value of the integer entered by the user
	 */
	public int getInt(int lowLimit, int highLimit) {
		boolean loop = true;
		int returnValue = 0;

		Scanner input = new Scanner(System.in);
		while (loop) {
			try {
				returnValue = input.nextInt();
				if ((returnValue >= lowLimit) && (returnValue <= highLimit)) {
					loop = false;
				} else {
					System.out.println("Selection does not exist.");
					loop = true;
				}
			} catch (InputMismatchException notInt) {
				invalidInput(input);
			}
		}
		return returnValue;
	}

	private void invalidInput(Scanner input) {
		System.out.println("Invalid input, try again.");
		input.next(); // This consumes the invalid token
	}

	/**
	 * Asks the user for a string and then returns it.
	 *
	 * @return A <code>String</code> object containing the characters entered by
	 * the user
	 */
	public String getString() {
		Scanner input = new Scanner(System.in);
		try {
			String returnValue = input.nextLine();
			return returnValue;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * Asks the user for a char and then returns it.
	 *
	 * @return A character entered by the user. If multiple characters are
	 * entered the first one will be returned.
	 */
	public char getChar() {
		boolean loop = true;
		char returnChar = '\u0000'; // initialize to default value

		Scanner input = new Scanner(System.in);

		while (loop) {
			try {
				returnChar = input.nextLine().charAt(0);
				loop = false;
			} catch (IndexOutOfBoundsException ex) {
				System.out.println("Invalid input, try again.");
				input.next(); // This consumes the invalid token
			}
		}
		return returnChar;
	}
}