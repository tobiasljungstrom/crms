package tools;

/**
 * This class takes in data and uses it for a menu system. The value 0 is used as exit value.
 *
 * @author Tobias Ljungstr&ouml;m
 */
public class ConsoleMenu extends Menu {

	public ConsoleMenu(String menuTitle, String... items) {
		super(menuTitle, items);
	}

	/**
	 * Asks the user for a number between 0 and the maximum number of menu items.
	 *
	 * @return The value entered by the user as an integer.
	 */
	@Override
	public int makeSelection() {
		ConsoleInput input = new ConsoleInput();

		System.out.println("Enter a number between 0 and " + super.getMenuItems().size() + ":");
		return input.getInt(0, super.getMenuItems().size());
	}

	/**
	 * Displays all items in the menu, with the value "Exit" assigned to the number 0.
	 */
	@Override
	public void displayMenu() {
		System.out.println("\n-- " + super.getTitle() + " --");

		for (int i = 0; i < super.getMenuItems().size(); i++) {
			System.out.println("[" + (i + 1) + "] " + super.getMenuItems().get(i));
		}
		
		System.out.println("\n[0] Exit");
	}
}