package tools;

import java.util.ArrayList;

/**
 * @author Tobias
 */
public abstract class Menu {

	/*Constructor*/
	public Menu(String title, String... item) {
		this.menuTitle = title;

		menuItems = new ArrayList<String>();

		for (int i = 0; i < item.length; i++) {
			menuItems.add(item[i]);
		}
	}

	public abstract int makeSelection();

	public abstract void displayMenu();
	
	/*Getters and setters*/

	public String getTitle() {
		return menuTitle;
	}

	public ArrayList<String> getMenuItems() {
		return menuItems;
	}

	/*Variables*/
	private String menuTitle;
	private ArrayList<String> menuItems;

}
