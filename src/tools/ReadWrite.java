package tools;

import main.Program;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Tobias on 2015-09-10.
 */
public class ReadWrite {

    private String filepath;

    public ReadWrite(String filepath) {
        this.filepath = filepath;
    }

    public void write(Program object) {
        try {
            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream outStream = new ObjectOutputStream(fileOut);

            outStream.writeObject(object);

            fileOut.close();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Program read() {
        try {
            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream inStream = new ObjectInputStream(fileIn);

            Program returnObject = (Program)inStream.readObject();

            fileIn.close();
            inStream.close();

            return returnObject;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
